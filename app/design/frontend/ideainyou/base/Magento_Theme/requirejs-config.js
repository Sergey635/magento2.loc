/*Copyright © Magento, Inc. All rights reserved. See COPYING.txt for license details.*/
let config = {
    map: {
        '*': {
            mageTemplateExample: 'Magento_Theme/js/mageTemplateExample',
            mageTemplateExampleExtend: 'Magento_Theme/js/mageTemplateExampleExtend',
            imageWidget: "Magento_Theme/js/imageWidget",
            loaderReview: "Magento_Theme/js/loaderReview",
            mapStores: "Magento_Theme/js/mapStores",
            storeForm: "Magento_Theme/js/storeForm",
            testMapService: "Magento_Theme/js/testMapService",
            openStoreForm: "Magento_Theme/js/openStoreForm",
            Autocomplete: "Magento_Theme/js/Autocomplete",
            storeComponent: "Magento_Theme/js/storeComponent"
}
    },
    config: {
        mixins: {
            'Magento_Theme/js/mageTemplateExample': {
                'Magento_Theme/js/mageTemplateExample-mixin': true
            }
        }
    }
};
