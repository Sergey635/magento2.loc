
define([
    'jquery',
    'underscore',
    'jquery-ui-modules/widget',
    'Magento_Ui/js/modal/modal',
    'mage/translate'

], function ($, _) {
    'use strict';



    $.widget('mage.imageWidget', $.mage.modal, {
        options: {
            attributesForm: {},
            attributesField: {},
            value: '',

            buttons: [{
                text: $.mage.__('Cancel'),
                class: 'action-secondary action-dismiss',

                /**
                 * Click handler.
                 */
                click: function () {
                    this.closeModal();
                }
            }, {
                text: $.mage.__('OK'),
                class: 'action-primary action-accept',

                /**
                 * Click handler.
                 */
                click: function () {
                    this.closeModal(true);
                }
            }]
        },

        /**
         * Create widget.
         */


        _create: function () {
            this._super();
            this.openModal();
        },

    });

        return $.mage.imageWidget;



});

