define([
    "jquery",
    "Magento_Ui/js/modal/modal",
    'uiComponent'

],function($, modal, Component) {
    'use strict';


    return Component.extend({
        defaults: {
            template: 'Magento_Theme/storeForm',
            type: 'popup',
            responsive: true,
            title: 'Create Store',
            modal: '#store_form',
            buttons: [{
                text: $.mage.__('Ok'),
                class: 'action primary',
                click: function () {
                    this.closeModal();
                }
            }],
        },
        initialize: function() {

            this._super()

            console.log('here')
        },

        componentClick: function (){

            console.log('click')
            $('#store-form-input').show

        }
    });

});
