define([
    "jquery",
    "Magento_Ui/js/modal/modal"
],function($, modal) {


    var options = {
        type: 'popup',
        responsive: true,
        title: 'Create Store',
        buttons: [{
            text: $.mage.__('Ok'),
            class: 'action primary',
            click: function () {
                this.closeModal();
            }
        }],

    };

    var popup = modal(options, $('#store_form'));
    $("#add_store").click(function() {
        $('#store_form').modal('openModal');
    });

});
