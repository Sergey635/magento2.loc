define(['jquery'], function ($) {
    'use strict';

    var mageTemplateExampleMixin = {
        /**
         * Added confirming for modal closing
         *
         * @returns {Element}
         */

        _create: function () {
            return this._super();
        }
    };

    return function (targetWidget) {
        $.widget('ideainyou.mageTemplateExample', targetWidget, mageTemplateExampleMixin);

        return $.ideainyou.mageTemplateExample;
    };
});
