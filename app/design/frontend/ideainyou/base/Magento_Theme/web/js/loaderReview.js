
    define([
        'jquery',
        'loader',
    ],  function ($) {
        $('body').trigger('processStart');

        setTimeout(function () {
            $('body').trigger('processStop');
        }, 5000);
});
