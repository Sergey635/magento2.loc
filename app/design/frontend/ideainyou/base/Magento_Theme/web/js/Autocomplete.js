define([
    'jquery',
    'underscore',
], function ($, _, ) {
    'use strict'
    $.widget('mage.autocomplete', {
        options: {

            autocomplete:'',
            address: '',

        },

         initMap: function() {
             var inputs = $('#address');
             autocomplete = new google.maps.places.Autocomplete(inputs);
             google.maps.event.addListener(autocomplete, 'place_changed', function () {
                 let place = autocomplete.getPlace();
                 if (!place.geometry) {
                     alert('Error');
                 }
                 let address = this.options.address;
                 if (place.address_components) {
                     address = [
                         (place.address_components[0] && place.address_components[0].short_name || ''),
                         (place.address_components[1] && place.address_components[1].short_name || ''),
                         (place.address_components[2] && place.address_components[2].short_name || '')
                     ].join(' ');
                 }
                 $('#latitude').value = place.geometry.location.lat();
                 $('#longitude').value = place.geometry.location.lng();
                 let city = '';
                 let street = '';
                 let house = '';
                 let region = '';
                 let country = '';
                 let tmp = '';
                 place.address_components.forEach(function (item) {
                     tmp = item.long_name;
                     if (item.types) {
                         item.types.forEach(function (t) {
                             switch (t) {
                                 case 'street_number':
                                     house = tmp;
                                     break;
                                 case 'route' :
                                     street = tmp;
                                     break;
                                 case 'administrative_area_level_1' :
                                 case 'administrative_area_level_2' :
                                     region = tmp;
                                     break;
                                 case 'country' :
                                     country = tmp;
                                     break;
                                 case 'postal_town' :
                                 case 'locality' :
                                     city = tmp;
                                     break;
                             }
                         });
                     }
                 });
                 $('#city').value = city;
                 $('#street').value = street;
                 $('#house').value = house;
                 $('#region').value = region;
                 $('#country').value = country;
             });

         }
         });

return $.mage.autocomplete;
});
