define([
    'jquery',
    'underscore',

], function ($, _, ) {
    $.widget('mage.storeForm', {
        options: {
            formSelector: '',
            submitUrl: '',
            messageSelector: '',
        },


        _create: function () {
            this._super();
            this._bindStore();

        },
        _bindStore: function () {
            $(this.options.formSelector).on('submit', this.storeClick.bind(this))
        },

        storeClick: function (e) {
            let self = this;
            e.preventDefault();
            $.ajax({

                url: this.options.submitUrl,
                type: 'GET',
                data: {
                    name: $('#name').val(),
                    city: $('#city').val(),
                    country: $('#country').val(),
                    state: $('#region').val(),
                    longitude: $('#longitude').val(),
                    latitude: $('#latitude').val()
                },

            }).done(function (data) {
                if (data.success) {
                    $(self.options.messageSelector).show();
                    $(self.options.formSelector)[0].reset();
                    $("#test_store_list").load(" #test_store_list");

                    setTimeout(function () {

                        $(self.options.messageSelector).hide();
                    }, 3000)
                } else {
                    alert({
                        title: $.mage.__('Something wrong'),
                        content: $.mage.__(data.error_message + '\nPlease, try again!')
                    });
                }
            }).error(function (qXHR, textStatus, errorThrown) {
                alert({
                    title: $.mage.__('Something wrong'),
                    content: $.mage.__('Please, try again!')
                });
                console.log(qXHR, textStatus, errorThrown)
            });
        }

    });

    return $.mage.storeForm;
});
