define([
    'uiComponent'
], function (Component) {
    'use strict';

    return Component.extend({
        defaults: {
            text: ''
        },
        initialize: function (){
            return this._super()
                .observe('text');
        },
        clickFunction: function (){
            console.log('click')
        }
    });
});
