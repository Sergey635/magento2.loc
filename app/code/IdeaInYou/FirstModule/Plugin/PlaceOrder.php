<?php

namespace IdeaInYou\FirstModule\Plugin;

use Magento\Sales\Model\Order;

class PlaceOrder
{
    public function beforeRegisterCancellation(Order $subject, $result, $comment = '', $graceful = true)
    {
        $subject->hold();
        $subject->save();

        return [$comment, $graceful];
    }

    public function afterGetShippingMethod(Order $subject, $result, $asObject = false)
    {
        $subject->hold();
        $subject->save();

        return $result;
    }

    public function aroundGetShippingMethod(Order $subject, callable $proceed, $asObject = false)
    {
        $asObject = true;

        $result = $proceed($asObject);

        $result->setData('total', 22);

        return $result;
    }
}
