<?php

namespace IdeaInYou\FirstModule\Model\Attribute\Source;

use IdeaInYou\Review\Api\ReviewRepositoryInterface;
use IdeaInYou\Review\Model\ResourceModel\Review\CollectionFactory;
use IdeaInYou\Review\Model\Review;

class SizeSource
{
    private CollectionFactory $collectionFactory;
    private ReviewRepositoryInterface $reviewRepository;

    public function __construct(
        CollectionFactory $collectionFactory,
        ReviewRepositoryInterface $reviewRepository
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->reviewRepository = $reviewRepository;
    }

    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        $options = [];

        foreach ($this->collectionFactory->create() as $review) {
            /** @var Review $review */
            $options[] = ['value' => (int) $review->getId(), 'label' => $review->getAuthor()];
        }

        return $options;
    }

    public function getOptionText($value)
    {
        return $this->reviewRepository->getById($value)->getContent();
    }

    public function setAttribute() {
        return $this;
    }
}
