<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IdeaInYou\Review\Model\ResourceModel\Stores;

use IdeaInYou\Review\Model\Stores;

/**
 * Stores collection
 *
 * @api
 * @since 100.0.2
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'store_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Stores::class,
            \IdeaInYou\Review\Model\ResourceModel\Stores::class
        );
    }
}
