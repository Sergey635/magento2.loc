<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace IdeaInYou\Review\Model\ResourceModel;

use IdeaInYou\Review\Api\StoresInterface;

class Stores extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{



    protected function _construct()
    {
        $this->_init(StoresInterface::TABLE_NAME, StoresInterface::STORE_ID);
    }

}
