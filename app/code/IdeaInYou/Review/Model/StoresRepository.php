<?php

namespace IdeaInYou\Review\Model;

use IdeaInYou\Review\Api\StoresRepositoryInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use IdeaInYou\Review\Model\ResourceModel\Stores as ResourceStores;
use \IdeaInYou\Review\Model\StoresFactory;

class StoresRepository implements StoresRepositoryInterface
{
    private StoresFactory $storesFactory;
    private ResourceStores $resource;

    public function __construct(
        StoresFactory $storesFactory,
        ResourceStores $resource
    ) {
        $this->resource = $resource;
        $this->storesFactory = $storesFactory;
    }

    public function getById($id)
    {
        $stores = $this->storesFactory->create();
        $this->resource->load($stores, $id);
        if (!$stores->getId()) {
            throw new NoSuchEntityException(__('Stores with id "%1" does not exist.', $id));
        }
        return $stores;
    }

    public function save(Stores $stores)
    {
        try {
            $this->resource->save($stores);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the store: %1',
                $exception->getMessage()
            ));
        }
        return $stores;
    }

    public function delete(Stores $stores)
    {
        try {
            $this->resource->delete($stores);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Store: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param $id
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $this->delete($this->getById($id));
    }
}
