<?php

namespace IdeaInYou\Review\Model;

use IdeaInYou\Review\Api\StoresInterface;
use Magento\Framework\DataObject\IdentityInterface;

class Stores extends \Magento\Framework\Model\AbstractModel implements StoresInterface, IdentityInterface
{
    const  CACHE_TAG = 'ideainyou_stores';

    protected $_eventPrefix = 'ideainyou_store';


    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\IdeaInYou\Review\Model\ResourceModel\Stores::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::STORE_ID);
    }

    public function setId($id)
    {
        return $this->setData(self::STORE_ID, $id);
    }

    public function getName()
    {
        return parent::getData(self::NAME);
    }

    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    public function getDescription()
    {
        return parent::getData(self::DESCRIPTION);
    }

    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }

    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    public function getState()
    {
        return parent::getData(self::STATE);
    }

    public function setState($state)
    {
        return $this->setData(self::STATE, $state);
    }

    public function getCity()
    {
        return parent::getData(self::CITY);
    }

    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    public function getPost()
    {
        return parent::getData(self::POST);
    }

    public function setPost($post)
    {
        return $this->setData(self::POST, $post);
    }

    public function getImage()
    {
        return parent::getData(self::IMAGE);
    }



    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getLongitude()
    {
        return parent::getData(self::LONGITUDE);
    }

    public function setLongitude($longitude)
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    public function getLatitude()
    {
        return parent::getData(self::LATITUDE);
    }

    public function setLatitude($latitude)
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    public function getStore($storeId = null)
    {
        return $this->getName();
    }

}
