define([
    'jquery'
], function ($) {
    'use strict';

    return $.extend({
        defaults: {
            text: ''
        },
        initialize: function (){
            return this._super()
                .observe('text');
        },
        clickMarker: function (){
            console.log('click')
        },

        __create: function (){
            console.log('hello click')
        }
    });
});
