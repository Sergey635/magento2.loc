<?php

namespace IdeaInYou\Review\Observer;

use IdeaInYou\Review\Api\StoresRepositoryInterface;
use IdeaInYou\Review\Model\StoresFactory;
use IdeaInYou\Review\Model\Stores;
use Magento\Sales\Model\Order;
use function _HumbugBoxe8a38a0636f4\React\Promise\_checkTypehint;


class Autocomplete implements \Magento\Framework\Event\ObserverInterface
{
    private StoresFactory $storesFactory;
    private StoresRepositoryInterface $storesRepository;

    public function __construct(
        StoresFactory $storesFactory,
        StoresRepositoryInterface $storesRepository
    )
    {
        $this->storesFactory = $storesFactory;
        $this->storesRepository = $storesRepository;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $store = $observer->getObject();
        $address = $_POST['address'];
        $address = str_replace(" ", "+", $address);
        $json = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false&language=en&key=AIzaSyDHWspkQVXb_6lJ06xD2yHIGTsg7Syzxz8");
        $json = json_decode($json);
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $lng = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        $store->setLatitude($lat)->setLongitude($lng);
        return $this;
    }

}
