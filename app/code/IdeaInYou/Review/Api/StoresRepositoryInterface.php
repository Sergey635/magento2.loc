<?php

namespace IdeaInYou\Review\Api;

use IdeaInYou\Review\Model\Stores;

interface StoresRepositoryInterface
{

    public function getById($id);

    public function save(Stores $stores);

    public function delete(Stores $stores);

    public function deleteById($id);


}
