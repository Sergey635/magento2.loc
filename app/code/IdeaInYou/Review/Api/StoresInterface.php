<?php

namespace IdeaInYou\Review\Api;

interface StoresInterface
{
    const TABLE_NAME = 'ideainyou_store';
    const STORE_ID = 'store_id';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const COUNTRY = 'country';
    const CITY = 'city';
    const STATE = 'state';
    const POST = 'post';
    const IMAGE = 'image';
    const LONGITUDE = 'longitude';
    const LATITUDE = 'latitude';

    public function getId();

    public function setId($id);

    public function getName();

    public function setName($name);

    public function getDescription();

    public function setDescription($description);

    public function getCountry();

    public function setCountry($country);

    public function getState();

    public function setState($state);

    public function getCity();

    public function setCity($city);

    public function getPost();

    public function setPost($post);

    public function getImage();

    public function setImage($image);

    public function getLongitude();

    public function setLongitude($longitude);

    public function getLatitude();

    public function setLatitude($latitude);

    public function getStore($storeId = null);

}
