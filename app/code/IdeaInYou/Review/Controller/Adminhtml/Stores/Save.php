<?php

namespace IdeaInYou\Review\Controller\Adminhtml\Stores;

use IdeaInYou\Review\Api\StoresRepositoryInterface;
use IdeaInYou\Review\Model\StoresFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;

/**
 * Save CMS block action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    private Registry $_coreRegistry;
    /**
     * @var StoresFactory|mixed
     */
    private $storesFactory;
    /**
     * @var StoresRepositoryInterface|mixed
     */
    private $storesRepository;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        StoresFactory $storesFactory = null,
        StoresRepositoryInterface $storesRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->storesFactory = $storesFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StoresFactory::class);
        $this->storesRepository = $storesRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(StoresRepositoryInterface::class);
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            if (empty($data['store_id'])) {
                $data['store_id'] = null;
            }

            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->storesFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->storesRepository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->storesRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the store.'));
                $this->dataPersistor->clear('ideainyou_store');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

            $this->dataPersistor->set('ideainyou_store', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Magento\Cms\Model\Block $model
     * @param array $data
     * @param \Magento\Framework\Controller\ResultInterface $resultRedirect
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect ==='continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }

}
