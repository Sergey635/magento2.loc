<?php
namespace IdeaInYou\Review\Controller\Adminhtml\Stores;

use IdeaInYou\Review\Api\StoresRepositoryInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class MassDelete
 */
class Delete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    private StoresRepositoryInterface $storesRepository;

    /**
     * @param Context $context
     * @param StoresRepositoryInterface $storesRepository
     */
    public function __construct(
        Context $context,
        StoresRepositoryInterface $storesRepository
    ) {
        parent::__construct($context);
        $this->storesRepository = $storesRepository;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $storeId = $this->getRequest()->getParam('id');

        if ($storeId){
            try {
                $this->storesRepository->deleteById($storeId);
                $this->messageManager->addSuccessMessage(__('Store (ID: 1) have been deleted.', $storeId));
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
