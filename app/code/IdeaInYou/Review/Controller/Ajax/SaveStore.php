<?php

namespace IdeaInYou\Review\Controller\Ajax;

use IdeaInYou\Review\Api\StoresRepositoryInterface;
use IdeaInYou\Review\Model\StoresFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Json as ResultJson;
use Magento\Framework\Controller\Result\JsonFactory as ResultJsonFactory;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

/**
 * Class SaveStore
 */
class SaveStore implements HttpGetActionInterface
{
    /**
     * @var JsonSerializer
     */
    private JsonSerializer $jsonSerializer;
    /**
     * @var ResultJsonFactory
     */
    private ResultJsonFactory $resultJsonFactory;
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;
    private StoresFactory $storesFactory;
    private StoresRepositoryInterface $storesRepository;

    public function __construct(
        JsonSerializer $jsonSerializer,
        ResultJsonFactory $resultJsonFactory,
        RequestInterface $request,
        StoresFactory $storesFactory,
        StoresRepositoryInterface $storesRepository
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->request = $request;
        $this->storesFactory = $storesFactory;
        $this->storesRepository = $storesRepository;
    }

    /**
     * @return ResultJson
     */
    public function execute()
    {
        if (!$this->request->isAjax()) {
            $data = [
                'success' => false,
                'error_message' => __('This is not $.ajax')
            ];

            return $this->resultJsonFactory->create()->setData($data);
        }

        try {
            $requestData = $this->request->getParams();

            $stores = $this->storesFactory->create();
            $stores->setData($requestData);

            $this->storesRepository->save($stores);

            $data = [
                'success' => true,
                'error_message' => '',
            ];
        } catch (\Exception $ex) {
            $data = [
                'success' => false,
                'error_message' => $ex->getMessage()
            ];
        }

        return $this->resultJsonFactory->create()->setData($data);
    }
}
