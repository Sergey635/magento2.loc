<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace IdeaInYou\Review\Block\Stores;

use IdeaInYou\Review\Model\ResourceModel\Stores\CollectionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Url\Helper\Data as UrlHelper;


class Stores extends \Magento\Framework\View\Element\Template
{
    /**
     * @var bool
     */
    protected $_storeInUrl;

    /**
     * @var \Magento\Framework\Data\Helper\PostHelper
     */
    protected $_postDataHelper;

    /**
     * @var UrlHelper
     */
    private $urlHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper
     * @param array $data
     * @param UrlHelper $urlHelper
     */

    private CollectionFactory $collectionFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        CollectionFactory $collectionFactory,
        array $data = [],
        UrlHelper $urlHelper = null
    ) {
        $this->_postDataHelper = $postDataHelper;
        parent::__construct($context, $data);
        $this->urlHelper = $urlHelper ?: ObjectManager::getInstance()->get(UrlHelper::class);
        $this->collectionFactory = $collectionFactory;
    }

    public function getStores()
    {
        $collection = $this->collectionFactory->create();

        return $collection->getItems();
    }


}
